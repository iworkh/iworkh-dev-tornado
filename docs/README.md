# Tornado

[IT更多资料](https://www.iworkh.com/manualIt/Category/scopeDev)

## 是什么

Tornado是一个python语言的web服务框架，它是基于社交聚合网站FriendFeed的实时信息服务开发而来的。


## 目标
掌握tornado核心知识。


## 内容
- tornado使用


## 本册对应代码地址

- [本册对应gitee地址](https://gitee.com/iworkh/iworkh-dev-tornado/)  
(如果开源的，可以点右上面的gitee/gitehub图片，就可以查看源码，还在整理中的资料，需要切换到feature分支才能查看到源码和文档)

## 本册常用链接

- [tornado官网文档](https://www.tornadoweb.org/en/stable/index.html)
- [tornado中文文档](https://tornado-zh.readthedocs.io/zh/latest/)
- [tornado书籍](http://shouce.jb51.net/tornado/)

## 更多IT手册

> https://www.iworkh.com
>
> 本栈覆盖 “前端/后端/运维/测试” 常见技术的IT手册，为您提供丰富且全面的IT学习手册，不用再为找IT学习资料而烦恼。内容从入门到高级再深入源码，由浅入深，由点到面，为您提供一套完整IT学习路线图及学习手册，让您从小白到精通，由菜鸟到大神。只要您有一颗好学之心，就能助您早日成"神"。
> 我们也期待任何有意朋友能加入我们，一起打造一套完整且全面的IT学习手册，能给需要的人员提供一点思路或者帮助，为每一位"IT修神"之路的人，推一把力，助一次攻，留下一点足迹......


## 参照

来源于很多地方，有github，有官网，也有各个博客或者公众号的等等。

(未经过本栈同意，不得以任何方式转发本栈整理的文档资料、以及倒卖本栈文档获取利益，一旦发现将追究法律责任)
