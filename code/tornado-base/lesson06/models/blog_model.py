import datetime

import peewee_async
from peewee import Model, DateTimeField, TextField, CharField

db_info = {'host': '127.0.0.1', 'user': 'iworkh', 'password': 'iworkh123', 'charset': 'utf8', 'port': 3306}

db = peewee_async.MySQLDatabase('iworkh_tornado', **db_info)


class BaseModel(Model):
    created_date = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db


class BlogModel(BaseModel):
    title = CharField(column_name='title', max_length=50)
    content = TextField(column_name='content')

    class Meta:
        table_name = 't_blog'
