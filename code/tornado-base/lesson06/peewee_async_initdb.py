
from lesson06.models.blog_model import BlogModel, db

if __name__ == '__main__':
    db.connect()
    model_list = [BlogModel]
    db.create_tables(model_list)
    print("end...")
