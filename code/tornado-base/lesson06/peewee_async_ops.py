import asyncio

import peewee_async

from lesson06.models.blog_model import db, BlogModel

objects = peewee_async.Manager(db)


async def save():
    blog = {'title': 'tornado入门', 'content': '详细内容请看文章'}
    await objects.create(BlogModel, **blog)
    print("保存成功")


async def get_all():
    all_blogs = await objects.execute(BlogModel.select())
    for blog in all_blogs:
        print("id: {}, 文章标题：{}".format(blog.id, blog.title))


async def delete():
    blog = BlogModel()
    blog.id = 1
    await objects.delete(blog)
    print("删除成功")

def create_table():
    BlogModel.create_table(True)

def drop_table():
    BlogModel.drop_table(True)


async def test():
    create_table()
    await save()
    print("*"*50)
    await get_all()
    print("*"*50)
    await delete()
    print("*"*50)
    await get_all()
    drop_table()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())
    print("end")
