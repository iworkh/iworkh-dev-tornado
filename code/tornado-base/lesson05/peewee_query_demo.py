from lesson05.models.user_model import User


def select_all():
    # modelSelect = User.select() # sql: select * from user
    fields = [User.username, User.phone]
    modelSelect = User.select(*fields)  # sql: select username, phone from user
    # 这还没有立即执行，返回的是modelSelect对象，当for或者execute时才会执行
    for user in modelSelect:
        print("{} --- {} --- {}".format(user.username, user.phone, user.is_admin))


def select_conditon():
    # 根据id取记录，如果id不存在，那么转化时候会报错
    user_res_get_by_id = User.get_by_id(1)
    print("get_by_id 结果：{}".format(user_res_get_by_id.username))

    user_res_get = User.get(User.id == 1)
    print("user_res_get 结果：{}".format(user_res_get.username))

    user_res_dic = User[1]
    print("user_res_dic 结果：{}".format(user_res_dic.username))

    print("*" * 50)

    # select * from user where username like %iworkh%
    modelSelect = User.select().where(User.username.contains('iworkh'))
    for user in modelSelect:
        print("{} --- {}".format(user.username, user.is_admin))

    print("*" * 50)

    # select * from user ordery by birthday desc
    modelSelect_order = User.select().order_by(User.birthday.desc())
    for user in modelSelect_order:
        print("{} --- {}".format(user.username, user.birthday))

    print("*" * 50)
    # 按每页3的大小分页，取第2页数据(从1开始计数)
    modelSelect_pagable = User.select().paginate(page=2, paginate_by=3)
    for user in modelSelect_pagable:
        print("{} --- {}".format(user.username, user.id))


if __name__ == '__main__':
    select_all()
    select_conditon()
