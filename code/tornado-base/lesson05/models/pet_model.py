from peewee import ForeignKeyField, CharField, IntegerField

from lesson05.models.base_model import BaseModel
from lesson05.models.user_model import User


class Pet(BaseModel):
    user = ForeignKeyField(User, backref='pets')
    name = CharField(index=True, column_name='name', max_length=50, verbose_name='名字')
    age = IntegerField(column_name='age', verbose_name='年龄')
    type = CharField(column_name='type', max_length=50, verbose_name='类型')
    color = CharField(column_name='color', max_length=50, verbose_name='颜色')
    description = CharField(column_name='description', max_length=500, verbose_name='描述')

    class Meta:
        table_name = "T_PET"
