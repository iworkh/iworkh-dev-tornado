from peewee import CharField, DateField, IntegerField, BooleanField

from lesson05.models.base_model import *

class User(BaseModel):
    username = CharField(column_name='username', unique=True, null=False, max_length=50, verbose_name="姓名")
    birthday = DateField(column_name='birthday', null=True, default=None, verbose_name='生日')
    phone = CharField(column_name='phone', max_length=11)
    mail = CharField(column_name='mail', max_length=50, verbose_name='邮件')
    gender = IntegerField(column_name='gender', null=False, default=1, verbose_name='姓别')
    is_admin = BooleanField(column_name='is_admin', default=False, verbose_name='是否是管理员')

    class Meta:
        table_name = "T_USER"