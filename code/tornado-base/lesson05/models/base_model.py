import datetime

from peewee import MySQLDatabase, Model, DateTimeField

db_info = {'host': '127.0.0.1', 'user': 'iworkh', 'password': 'iworkh123', 'charset': 'utf8', 'port': 3306}

db = MySQLDatabase('iworkh_tornado', **db_info)

class BaseModel(Model):
    created_date = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db
