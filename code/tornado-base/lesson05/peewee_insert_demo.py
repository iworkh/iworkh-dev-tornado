import datetime

from lesson05.models.user_model import User

userList = [
    {'username': 'zhangsan', 'birthday': '1988-08-08', 'gender': 1, 'mail': 'zhangsan@qq.com', 'phone': '111',
     'is_admin': False},
    {'username': 'lisi', 'birthday': datetime.date(1999, 8, 8), 'gender': 1, 'mail': 'lisi@qq.com', 'phone': '222',
     'is_admin': False},
    {'username': 'wangwu', 'birthday': datetime.date(2025, 8, 8), 'gender': 0, 'mail': 'wangwu@qq.com', 'phone': '333',
     'is_admin': False},
    {'username': 'admin', 'birthday': datetime.date(2000, 8, 8), 'gender': 1, 'mail': 'admin@qq.com', 'phone': '444',
     'is_admin': True}
]


def save():
    user = User()
    user.username = 'iworkh_save'
    user.birthday = datetime.date(1988, 8, 8)
    user.gender = 0
    user.mail = "157162006@qq.com"
    user.phone = "888888888"
    user.is_admin = True

    # 使用对象调用方法
    row = user.save()
    print("save的返回值是值:{}".format(row))


def save_list():
    for item in userList:
        user = User(**item)
        user.save()


def create():
    user_dic = {
        'username': 'iworkh_create2',
        'birthday': datetime.date(1989, 8, 8),
        'gender': 1,
        'mail': '157162006@qq.com',
        'phone': '888888888',
        'is_admin': False
    }

    # 使用类调用方法
    row = User.create(**user_dic)
    print("create的返回值值:{}".format(row))


if __name__ == '__main__':
    save()
    create()
    save_list()
    print("end...")
