from lesson05.models.base_model import db
from lesson05.models.pet_model import Pet
from lesson05.models.user_model import User

if __name__ == '__main__':
    db.connect()
    table_list = [User, Pet]
    db.create_tables(table_list)
    print("end...")
