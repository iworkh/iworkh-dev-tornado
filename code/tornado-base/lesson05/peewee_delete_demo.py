from lesson05.models.user_model import User


def delete_by_id():
    # 类操作
    User.delete_by_id(3)


def delete_instance():
    user = User()
    user.id = 4
    # 对象操作，recursive参数控制关联数据是否删除
    user.delete_instance()


if __name__ == '__main__':
    delete_by_id()
    delete_instance()
    print("end...")
