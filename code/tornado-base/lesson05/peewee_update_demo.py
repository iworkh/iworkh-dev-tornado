from lesson05.models.user_model import User


def upate():
    # 使用save方法即可更新，只要传的值中还有id
    user_res_get_by_id = User.get_by_id(1)
    user_res_get_by_id.username = user_res_get_by_id.username + '_update'
    # 从数据中取处记录，并修改值后更新数据库
    user_res_get_by_id.save()

if __name__ == '__main__':
   upate()
