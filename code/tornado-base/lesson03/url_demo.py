from tornado import web, ioloop


class IndexHandler(web.RequestHandler):
    async def get(self, *args, **kwargs):
        self.write("首页")


class UserHandler(web.RequestHandler):
    async def get(self, name, *args, **kwargs):
        self.write("hello world, {}".format(name))


class ProductHandler(web.RequestHandler):
    def get(self, name, count, *args, **kwargs):
        self.write("购买{}个{}".format(count, name))


class DivHandler(web.RequestHandler):
    #  one,two这个要跟url里的(?P<one>\d+)/(?P<two>\d+)里的名称匹配
    def get(self, one, two, *args, **kwargs):
        try:
            result = int(one) / int(two)
            self.write("{}/{}={}".format(one, two, result))
        except ZeroDivisionError:
            # 发生错误，会跳转
            self.redirect(self.reverse_url('error_page', 500))
            pass


class ErrorHandler(web.RequestHandler):
    def get(self, code, *args, **kwargs):
        self.write("发生错误：error_code: {}".format(code))


url_map = [
    ("/user/(\w+)/?", UserHandler),
    ("/product/(\w+)/(\d+)/?", ProductHandler),
    ("/calc/div/(?P<one>\d+)/(?P<two>\d+)/?", DivHandler),
    web.URLSpec("/index/?", IndexHandler, name="index"),
    web.URLSpec("/error/(?P<code>\d+)/?", ErrorHandler, name="error_page")
]

if __name__ == '__main__':
    app = web.Application(url_map, debug=True)
    app.listen(8888)
    print('started...')
    ioloop.IOLoop.current().start()