from tornado import options, web, ioloop

options.define("port", default="8888", type=int, help="端口号")
options.define("static_path", default="static", type=str, help="静态资源路径")


class IndexHandler(web.RequestHandler):
    async def get(self):
        await self.finish('welcome to iworkh !!!')

url_map = [
    ("/?", IndexHandler)
]

setting = {
    'debug': True
}

if __name__ == '__main__':
    # 命令行方式
    # options.parse_command_line()
    # 配置文件方式
    options.parse_config_file('config.ini')

    app = web.Application(url_map, **setting)
    app.listen(options.options.port)
    ioloop.IOLoop.current().start()
