from typing import Optional

from tornado import web, ioloop


class OrderHandler(web.RequestHandler):

    def get(self):
        data = {
            'title': '书本列表',
            'orderList': [
                {'id': 1, 'name': 'java', 'price': 80, 'count': 1,
                 'link_to': '<a href="http://www.baidu.com">查看</a>'},
                {'id': 2, 'name': 'springboot', 'price': 100, 'count': 2,
                 'link_to': '<a href="http://www.baidu.com">查看</a>'},
                {'id': 3, 'name': 'springcloud', 'price': 120, 'count': 1,
                 'link_to': '<a href="http://www.baidu.com">查看</a>'},
            ]
        }
        self.render("tp_order.html", **data)


class FooterUIMoudle(web.UIModule):

    def render(self):
        return self.render_string("uimodules/tp_footer.html")

    def embedded_css(self) -> Optional[str]:
        css = '''
        .content { background: gray}
        a {text-decoration-line: none}
        '''
        return css


url_handlers = [
    (r"/order", OrderHandler)
]

settings = {
    "debug": True,
    "template_path": 'public/templates',
    "static_path": 'public/static',
    "static_url_prefix": "/static/",
    "ui_modules": {
        'FooterUIMoudle': FooterUIMoudle
    }
}

if __name__ == '__main__':
    app = web.Application(url_handlers, **settings)
    app.listen(8888)
    print("started successfully")
    ioloop.IOLoop.current().start()
