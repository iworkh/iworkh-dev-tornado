from tornado import web, ioloop


class ProcessDemoHandler(web.RequestHandler):

    # initialize方法同步
    def initialize(self, dbinfo):
        print("initialize...")
        self.dbinfo = dbinfo
        print("数据库host:{}".format(self.dbinfo['db_host']))
        pass

    async def prepare(self):
        print("prepare...")
        pass

    async def get(self):
        print("get...")
        self.write("success!!!")
        pass

    def on_finish(self):
        print("finish...")
        pass


init_param = {
    'dbinfo': {
        'db_host': 'localhost',
        'db_port': 3306,
        'db_user': 'root',
        'db_password': '123',
    }
}

url_handers = [
    (r"/?", ProcessDemoHandler, init_param),
]

if __name__ == '__main__':
    app = web.Application(url_handers, debug=True)
    app.listen(8888)
    print("started...")
    ioloop.IOLoop.current().start()
