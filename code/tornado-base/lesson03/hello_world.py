import time

from tornado import web
from tornado.ioloop import IOLoop


class HelloWorldHandlerOne(web.RequestHandler):
    async def get(self, *args, **kwargs):
        # 别在handler中写同步的代码,会被阻塞的
        time.sleep(5)
        self.write("hello world---one")


class HelloWorldHandlerTwo(web.RequestHandler):
    def get(self, *args, **kwargs):
        self.write("hello world---two")


url_map = [
    ("/test1", HelloWorldHandlerOne),
    ("/test2", HelloWorldHandlerTwo)
]

if __name__ == '__main__':
    app = web.Application(url_map, debug=True)
    app.listen(8888)
    IOLoop.current().start()
