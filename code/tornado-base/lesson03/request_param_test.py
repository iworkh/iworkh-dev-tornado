import requests

# 1.
# url = "http://localhost:8888/url?name=zhangsan&&age=20"
# resp = requests.request('GET', url).content.decode("utf8")


# 2.
# url = "http://localhost:8888/form"
# data = {
#     "name": "zhangsan",
#     "age": 20
# }
# resp = requests.request('POST', url, data=data).content.decode("utf8")


# 3.
# url = "http://localhost:8888/jsonwithformheaders"
# headers = {"Content-Type": "application/x-www-form-urlencoded;"}
# data = {
#     "name": "zhangsan",
#     "age": 20
# }
# resp = requests.request('POST', url, headers=headers, data=data).content.decode("utf8")

# 4.
url = "http://localhost:8888/json"
data = {
    "name": "zhangsan",
    "age": 20
}
resp = requests.request('POST', url, json=data).content.decode("utf8")

print(resp)
