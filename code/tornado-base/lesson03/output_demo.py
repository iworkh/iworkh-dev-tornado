import asyncio

from tornado import web, ioloop


class OutputDemoHandler(web.RequestHandler):
    async def get(self):
        self.set_status(200)
        self.write("error!!!")
        self.write("warning!!!")
        self.write("<br/>")
        await self.flush()
        await asyncio.sleep(5)
        self.write("success!!!")
        await self.finish("done")


url_handers = [
    (r"/?", OutputDemoHandler),
]

if __name__ == '__main__':
    app = web.Application(url_handers, debug=True)
    app.listen(8888)
    print("started...")
    ioloop.IOLoop.current().start()
