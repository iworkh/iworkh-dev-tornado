import json

from tornado import web, ioloop


class UrlParamHandler(web.RequestHandler):
    async def get(self):
        name = self.get_query_argument("name")
        age = self.get_query_argument("age")
        self.write("name: {}, age: {}".format(name, age))

        self.write('<br/>')
        names = self.get_query_arguments("name")
        ages = self.get_query_arguments("age")
        self.write("names: {}, ages: {}".format(names, ages))


class FormParamHandler(web.RequestHandler):
    async def post(self):
        name = self.get_argument("name")
        age = self.get_argument("age")
        self.write("name: {}, age: {}".format(name, age))

        self.write('<br/>')
        names = self.get_arguments("name")
        ages = self.get_arguments("age")
        self.write("names: {}, ages: {}".format(names, ages))


class JsonWithFormHeadersParamHandler(web.RequestHandler):
    async def post(self):
        name = self.get_body_argument("name")
        age = self.get_body_argument("age")
        self.write("name: {}, age: {}".format(name, age))


class JsonParamHandler(web.RequestHandler):
    async def post(self):
        parm = json.loads(self.request.body.decode("utf8"))
        name = parm["name"]
        age = parm["age"]
        self.write("name: {}, age: {}".format(name, age))


url_handers = [
    ("/url", UrlParamHandler),
    ("/form", FormParamHandler),
    ("/jsonwithformheaders", JsonWithFormHeadersParamHandler),
    ("/json", JsonParamHandler)
]

if __name__ == '__main__':
    app = web.Application(url_handers, debug=True)
    app.listen(8888)
    print("started...")
    ioloop.IOLoop.current().start()
