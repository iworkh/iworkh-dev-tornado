from tornado import web, ioloop


class SettingsDemoHandler(web.RequestHandler):
    async def get(self):
        await self.finish("success")


url_handers = [
    (r"/?", SettingsDemoHandler),
]

settings = {
    'debug': True,
    'static_path': 'public/static',
    'static_url_prefix': '/static/'
}

if __name__ == '__main__':
    app = web.Application(url_handers, **settings)
    app.listen(8888)
    print("started...")
    ioloop.IOLoop.current().start()
