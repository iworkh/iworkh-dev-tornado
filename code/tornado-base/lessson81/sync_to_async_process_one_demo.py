import time
from concurrent.futures.process import ProcessPoolExecutor

from tornado import gen, web, ioloop

executor = ProcessPoolExecutor(max_workers=2)

def sleep():
    print("休息...start")
    time.sleep(5)
    print("休息...end")
    return 'ok'


class SyncToAsyncProcessHandler(web.RequestHandler):

    @gen.coroutine
    def get(self):
        rest = yield executor.submit(sleep)
        self.write(rest)


url_map = [
    ("/?", SyncToAsyncProcessHandler)
]

if __name__ == '__main__':
    app = web.Application(url_map, debug=True)
    app.listen(8888)
    print('started...')
    ioloop.IOLoop.current().start()
