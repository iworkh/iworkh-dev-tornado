# coding=utf-8

# 用法
import logging
from concurrent.futures import ProcessPoolExecutor
import os, time, random

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [*] %(processName)s  %(message)s"
)


def task (n):
    logging.info('%s is runing' % os.getpid())
    time.sleep(random.randint(1, 3))
    return n ** 2


if __name__ == '__main__':
    start = time.time()
    executor = ProcessPoolExecutor(max_workers=3)
    futures = []
    for i in range(11):
        future = executor.submit(task, i)
        futures.append(future)
    for future in futures:
        logging.info(future.result())
    executor.shutdown(True)
    logging.info(time.time() - start)

