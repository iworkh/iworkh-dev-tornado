import requests

url = "http://localhost:8888/order"

# 1
# data={
#     'id':'1'
# }
# resp = requests.get(url, data=data).content.decode("utf8")

# 2.
data = {
    'name':'springboot',
    'price': 100.0,
    'count':2
}
resp = requests.post(url, data=data).content.decode("utf8")

# 3
# data = {
#     'id': 1,
#     'name':'java',
#     'price': 60.0,
#     'count':1
# }
# resp = requests.put(url, data=data).content.decode("utf8")

print(resp)
