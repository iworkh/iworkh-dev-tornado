import aiomysql
from tornado import web, ioloop


class OrderHandler(web.RequestHandler):
    def initialize(self, db_info) -> None:
        self.db_info = db_info

    async def get(self):
        id = self.get_argument("id", None)
        if not id:
            raise Exception("id can not None")
        db_id = db_name = db_price = db_count = None
        async with aiomysql.create_pool(host=self.db_info["db_host"], port=self.db_info["db_port"],
                                        user=self.db_info["db_user"], password=self.db_info["db_password"],
                                        db=self.db_info["db_name"], charset=self.db_info["db_charset"]) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    sql = "SELECT `id`, `name`, `price`, `count` FROM TB_ORDER WHERE id = {};".format(id)
                    print(sql)
                    await cur.execute(sql)
                    result = await cur.fetchone()
                    try:
                        if (result):
                            db_id, db_name, db_price, db_count = result
                    except Exception:
                        pass

            self.write("success, {}, {}, {}, {}".format(db_id, db_name, db_price, db_count))

    async def post(self):
        db_name= self.get_argument("name")
        db_price= self.get_argument("price")
        db_count= self.get_argument("count")

        async with aiomysql.create_pool(host=self.db_info["db_host"], port=self.db_info["db_port"],
                                        user=self.db_info["db_user"], password=self.db_info["db_password"],
                                        db=self.db_info["db_name"], charset=self.db_info["db_charset"]) as pool:

            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    sql = "INSERT INTO TB_ORDER (`name`, `price`, `count`) VALUES ('{}','{}','{}');"\
                        .format(db_name, db_price, db_count)
                    print(sql)
                    await cur.execute(sql)
                    await conn.commit()
            self.write("save successfully")


    async def put(self):
        db_id= self.get_argument("id")
        db_name= self.get_argument("name")
        db_price= self.get_argument("price")
        db_count= self.get_argument("count")

        async with aiomysql.create_pool(host=self.db_info["db_host"], port=self.db_info["db_port"],
                                        user=self.db_info["db_user"], password=self.db_info["db_password"],
                                        db=self.db_info["db_name"], charset=self.db_info["db_charset"]) as pool:


            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    sql = "UPDATE TB_ORDER SET `name` = '{}', `price` = '{}', `count` = '{}' WHERE `id` = {};"\
                        .format(db_name, db_price, db_count, db_id)
                    print(sql)
                    await cur.execute(sql)
                    await conn.commit()
            self.write("save successfully")

initParam = {
    "db_info": {
        'db_host': 'localhost',
        'db_name': 'iworkh_tornado',
        'db_port': 3306,
        'db_user': 'iworkh',
        'db_password': 'iworkh123',
        'db_charset': 'utf8'
    }
}

url_handlers = [
    (r"/order/?", OrderHandler, initParam)
]

if __name__ == '__main__':
    app = web.Application(url_handlers, debug=True)
    app.listen(8888)
    print("started successfully")
    ioloop.IOLoop.current().start()
