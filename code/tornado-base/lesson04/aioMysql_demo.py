import asyncio

import aiomysql


async def test_example(loop):
    pool = await aiomysql.create_pool(host='localhost', port=3306,
                                      user='iworkh', password='iworkh123',
                                      db='iworkh_tornado', loop=loop)
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute("SELECT 42;")
            print(cur.description)
            (r,) = await cur.fetchone()
            assert r == 42
    pool.close()
    await pool.wait_closed()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test_example(loop))
