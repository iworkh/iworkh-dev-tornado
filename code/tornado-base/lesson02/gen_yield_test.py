def yield_method():
    print("do start...")
    yield 1
    print("do a...")
    res1 = yield 2
    print("do b...res1: {}".format(res1))
    res2 = yield 3
    print("do c...res2:{}".format(res2))
    yield 4


def call_hand():
    resp = yield_method()
    print(next(resp))  # 运行到 yield 1处，返回 1
    print(resp.__next__())  # 从yield 1开始运行，到 yield 2处，返回 2
    print(resp.send("got it"))  # 从yield 2开始运行，并把send值给yield 2的变量，到 yield 3处，返回 3
    print(next(resp))  # 从yield 3开始运行，到 yield 4处，返回 4

def call_for():
    for item in yield_method():
        print(item)


if __name__ == '__main__':
    print(yield_method())
    print("*" * 30)
    call_hand()
    print("*" * 30)
    call_for()
