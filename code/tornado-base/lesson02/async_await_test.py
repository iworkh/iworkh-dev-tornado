import asyncio
from asyncio import coroutine as pycoroutine
from time import sleep, time

from tornado.gen import coroutine as tcoroutine

async def async_method():
    await asyncio.sleep(2)
    print("do a...")
    await await_sleep_one_min()
    print("do b...python的async和await")
    await t_yield_sleep_one_min()
    print("do c...tornado装饰器")
    await py_yield_from_sleep_one_min()
    print("do d...python装饰器")
    await asyc_sleep_one_min()
    print("do e...")


# 不推荐使用, tornado装饰器方式
@tcoroutine
def t_yield_sleep_one_min():
    print("等了又等--1")
    yield sleep(1)
    print("等了又等--2")
    yield sleep(1)
    print("等了又等--3")
    yield sleep(1)


# 不推荐使用，python装饰器方式，python的旧语法
@pycoroutine
def py_yield_from_sleep_one_min():
    yield from asyncio.sleep(1)


# 推荐使用（async与await组合，因为asyncio.sleep也是个异步代码，需要用await）
async def await_sleep_one_min():
    await asyncio.sleep(1)


# 推荐使用（async单独使用，因为sleep不是个异步方法，不需要用await）
async def asyc_sleep_one_min():
    print('asyc_sleep_one_min----waiting')
    sleep(1)


if __name__ == '__main__':
    start = time()
    #  1.通过python的asyncio来run
    # 1-a. 一直run
    asyncio.ensure_future(async_method())
    asyncio.get_event_loop().run_forever()

    # 1-2. run完就结束
    # asyncio.run(async_method())
    # asyncio.get_event_loop().run_until_complete(async_method())

    #  2.通过tornado的IOLoop来run (执行某个协程后停止事件循环)
    # IOLoop.current().run_sync(async_method)

    end = time()
    print('cost time = ' + str(end - start))
