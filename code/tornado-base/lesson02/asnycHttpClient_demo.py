import asyncio

from tornado import httpclient


def get_url(url):
    http_client = httpclient.HTTPClient()
    resp = http_client.fetch(url)
    print(resp.body.decode("utf8"))


async def async_get_url(url):
    http_client = httpclient.AsyncHTTPClient()
    resp = await http_client.fetch(url)
    print(resp.body.decode("utf8"))


if __name__ == '__main__':
    web_site = "https://iworkh.gitee.io/blog/"
    asyncio.run(async_get_url(web_site))
    print("*" * 50)
    get_url(web_site)
