import socket

from selectors import DefaultSelector, EVENT_WRITE, EVENT_READ

selector = DefaultSelector()

class CallBackRequestSite:
    def get_url(self, url):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.setblocking(False)
        self.host = url
        self.data = b""
        try:
            self.client.connect((self.host, 80))
        except BlockingIOError as e:
            print("连接中...干些其他事")
            pass

        selector.register(self.client.fileno(), EVENT_WRITE, self.connected)

    def connected(self, key):
        selector.unregister(key.fd)
        self.client.send("GET {} HTTP/1.1\r\nHost:{}\r\nConnection:close\r\n\r\n".format("/", self.host).encode("utf8"))
        selector.register(self.client.fileno(), EVENT_READ, self.read_data)

    def read_data(self, key):
        recv_data = self.client.recv(1024)
        if recv_data:
            self.data += recv_data
        else:
            selector.unregister(key.fd)
            print(self.data.decode("utf8"))

def loop():
    while 1:
        # 根据系统是否支持，使用epoll还是select，优先epoll。默认阻塞，有活动连接就返回活动的连接列表
        ready = selector.select()
        for key, mask in ready:
            callback = key.data
            callback(key)


if __name__ == '__main__':
    demo = CallBackRequestSite()
    demo.get_url("www.baidu.com")
    loop()
