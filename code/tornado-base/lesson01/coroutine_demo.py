from tornado.gen import coroutine
from tornado.ioloop import IOLoop


@coroutine
def query():
    yield "first"
    yield "second"
    yield 3
    yield True
    return "done"

@coroutine
def divide(x, y):
    return x / y


def bad_call():
    # 这里应该抛出一个 ZeroDivisionError 的异常, 但事实上并没有
    # 因为协程的调用方式是错误的.
    divide(1, 0)

@coroutine
def good_call():
    # yield 将会解开 divide() 返回的 Future 并且抛出异常
    yield divide(1, 0)

if __name__ == '__main__':
    # bad_call()

    # call = good_call()
    # for item in call:
    #     print(item)

    # IOLoop.current().spawn_callback(divide, 1, 0)
    IOLoop.current().run_sync(lambda: divide(1, 0))

