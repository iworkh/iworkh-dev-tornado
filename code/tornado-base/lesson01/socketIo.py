import socket

# AF_INET：服务器之间网络通信，SOCK_STREAM 流式socket
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 设置非阻塞
client.setblocking(False)

# 传个tuple
address = "www.baidu.com"
try:
    client.connect(("www.baidu.com", 80))
except BlockingIOError as e:
    print("连接中...干些其他事")
    pass

while True:
    try:
        client.send("GET {} HTTP/1.1\r\nHost:{}\r\nConnection:close\r\n\r\n".format("/", address).encode("utf8"))
        print("连接成功")
        break
    except OSError as e:
        pass

# 定义bytes
data = b""

while 1:
    try:
        rec = client.recv(512)
        if rec:
            data += rec
        else:
            print(data.decode("utf8"))
            break
    except BlockingIOError as e:
        pass